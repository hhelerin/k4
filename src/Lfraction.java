// https://beginnersbook.com/2018/09/java-program-to-find-gcd-of-two-numbers/
//https://www.baeldung.com/java-compare-long-values
//https://www.geeksforgeeks.org/how-to-override-compareto-method-in-java/
//https://www.geeksforgeeks.org/convert-given-float-value-to-equivalent-fraction/
//https://www.dummies.com/education/math/algebra/how-to-convert-decimals-to-fractions/
//https://www.vogella.com/tutorials/JavaAlgorithmsEuclid/article.html


import java.util.Objects;

/** This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {

   }

   private long numerator;
   private long denominator;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      if(b == 0){
         throw new RuntimeException("Invalid input: " + b + "\nCan't divide with zero.");
      }
      else if(a == 0) {
         this.numerator = 0;
         this.denominator = 1;
      }else if (b == 1){
         this.numerator = a;
         this.denominator = b;
      }else if (b < 0){
         this.numerator = a * -1;
         this.denominator = b * -1;
      }
      else {
            long cd = getGreatestCommonDivisor(a, b);
            this.numerator = a/cd;
            this.denominator = b/cd;
         }
      }


   private static long getGreatestCommonDivisor(long a, long b){
      if (b == 0) {
         return java.lang.Math.abs(a);
      }
      return getGreatestCommonDivisor(b, java.lang.Math.abs(a) % b);
   }


   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return numerator;
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() { 
      return denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      if(numerator == 0){
         return "0";
      }
      return numerator + "/" + denominator;
   }

   /** To the power of.
    * @return fraction that's multiplied with itself n times.
    */
   public Lfraction pow(int n){
      switch (n){
         case 0:
            return new Lfraction(1,1);

         case 1:
            return new Lfraction(this.numerator, this.denominator);

         case -1:
            return this.inverse();

         default:
            if( n > 1){
               return this.times(this.pow(n-1));
            }
            else {
               return this.pow(Math.abs(n)).inverse();
            }
      }
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      if (!(m instanceof Lfraction)) {
         return false;
      } else {
         Lfraction other = (Lfraction) m;
         return this.compareTo(other) == 0;
      }
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Objects.hash(numerator, denominator);
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      long firstNumerator = numerator * m.denominator;
      long secondNumerator = m.numerator * denominator;

      return new Lfraction(firstNumerator + secondNumerator, denominator * m.denominator);
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      return new Lfraction(this.numerator * m.numerator, this.denominator * m.denominator);
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      Lfraction ans;
      if(numerator < 0){
         ans = new Lfraction(-denominator, -numerator);
      }else if( numerator == 0){
         throw new IllegalArgumentException("Invalid input. Can't divide with zero.");
      }else {
         ans = new Lfraction(denominator, numerator);
      }
      return ans;
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(-numerator, denominator);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      Lfraction opposite = m.opposite();
      return this.plus(opposite);
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      try{
         return this.times(m.inverse());
      } catch (RuntimeException r) {
         throw new IllegalArgumentException("Invalid input. Can't divide with zero.");
      }
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      long firstNumerator = numerator * m.denominator;
      long secondNumerator = m.numerator * denominator;

      if (firstNumerator > secondNumerator) {
         return 1;
      }
      else if (firstNumerator < secondNumerator) {
         return -1;
      }
      else {
         return 0;
      }
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {

      return new Lfraction(this.numerator, this.denominator);
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
      long ans = 0L;

      long num = Math.abs(numerator);
      long den = denominator;

      while(num >= den) {
         num = num - den;
         ans++;
      }
      if(numerator < 0){
         return -ans;
      }
      return ans;
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      long num = Math.abs(numerator);
      long den = denominator;

      if(numerator > 0){
         while(num >= den) {
            num = num - den;
         }
         return new Lfraction(num, den);

      }else {
         Lfraction  ans = this;
         num = den;

         for (int i = 0; i < Math.abs(this.integerPart()); i++) {
            ans = this.plus(new Lfraction(num, den));
            num = num + den;

         } return ans;
      }
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return numerator / (double)denominator;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      return new Lfraction(Math.round(f * d), d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      String[] expression = s.split("/");

      try{
         return new Lfraction(Long.parseLong(expression[0]), Long.parseLong(expression[1]));
      }catch (NumberFormatException n){
         throw new RuntimeException("Invalid input: " + s + "\n Expecting a fraction in format 1/2." );
      }
   }
}

